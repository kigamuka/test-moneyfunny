<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <title>Moneyfunny test task</title>
  <style>
    body, html {
      height: 100%;
    }

    * {
      box-sizing: border-box;
    }

    .bg-img {
      background-image: url("<?php echo $previewUrl; ?>");
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
      height: 360px;
      width: 640px;
    }

    /* Add styles to the form container */
    .container {
      /*position: absolute;*/
      /*left: 25%;*/
      /*margin: 20px;*/
      /*padding: 16px;*/

      margin-left: auto;
      margin-right: auto;
      width: 50%;
      height: 50%;
      padding-top: 25%;
    }

    /* Set a style for the submit button */
    .btn {
      background-color: #4CAF50;
      color: white;
      padding: 16px 20px;
      border: none;
      cursor: pointer;
      width: 100%;
      opacity: 0.9;
    }

    .btn:hover {
      opacity: 1;
    }
  </style>
</head>
<body id="home">

<h1><?php echo $channelId; ?></h1>
<div class="bg-img" id="previewBox">
  <form action="/action_page.php" class="container" id="playForm">
    <button type="button" class="btn" id="playButton">Play</button>
  </form>
</div>
<div id="playerBox" data-html='<?php echo $twitch->getPlayer(); ?>'></div>
</body>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="script.js"></script>
</html>