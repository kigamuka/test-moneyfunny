function swapBtnForPlayer() {
  var $playerBox = $('#playerBox');
  var html = $playerBox.data('html');

  $('#previewBox').html(html);
}

$('#playButton').on('click', function () {
  swapBtnForPlayer();
});