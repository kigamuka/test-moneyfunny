<?php

/**
 * @author Vitaliy Nesterko <kigamuka@gmail.com>
 */

namespace Main\Exception\Marker\Functional;

use Main\Exception\Marker;

interface BrokenBusinessLogicException extends Marker\Base\ServiceLayerException, Marker\Base\DataLayerException, Marker\Base\ControllerLayerException
{
}
