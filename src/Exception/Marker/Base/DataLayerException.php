<?php

/**
 * @author Vitaliy Nesterko <kigamuka@gmail.com>
 */

namespace Main\Exception\Marker\Base;

interface DataLayerException extends ServiceLayerException
{
}
