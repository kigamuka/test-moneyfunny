<?php

namespace Main\Exception;

use Main\Exception\Marker\Base\ServiceLayerException;

class InvalidResponseException extends CurlException implements ServiceLayerException
{
}