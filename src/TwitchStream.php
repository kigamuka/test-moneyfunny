<?php

namespace Main;

use Main\Exception\CurlException;
use Main\Exception\InvalidResponseException;

/**
 * Class TwitchStream
 * @package Main
 */
class TwitchStream
{
    /**
     * @var mixed
     */
    private $channel;

    /**
     * TwitchStream constructor.
     * @param string $mayBeChannelId
     */
    public function __construct($mayBeChannelId = '')
    {
        if ($mayBeChannelId) {
            $this->channel = $mayBeChannelId;
        }
    }

    /**
     * @return string
     */
    public function getPlayer()
    {
        return '<iframe src="https://player.twitch.tv/?channel=' . $this->channel . '" frameborder="0" allowfullscreen="true" scrolling="no" height="512" width="800"></iframe>';
    }

    /**
     * @return mixed
     * @throws CurlException
     * @throws InvalidResponseException
     */
    public function getPreview()
    {
        $data = $this->request($this->getStreamUrl());
        if (null === $data['stream']) {
            $data['offline'] = true;

            return $data;
        }

        return $data['stream']['preview']['large'];
    }

    /**
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param $channel
     * @return $this
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Готовая функция для работы с Twitch v5 API
     * @param $url
     * @param $post
     * @return bool|mixed
     */
    public function cURL($url, $post = null)
    {
        $url_self = 'http';
        if (in_array('HTTPS', $_SERVER) && $_SERVER["HTTPS"] == "on") {
            $url_self .= "s";
        }
        $url_self .= "://" . $_SERVER["SERVER_NAME"];
        if ($_SERVER["SERVER_PORT"] != "80") {
            $url_self .= ":" . $_SERVER["SERVER_PORT"];
        }

        if (substr($url_self, -1) != '/') {
            $url_self = $url_self . '/';
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Client-ID: b3rudj39b54n6mdai2hndi84fets2tm'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_REFERER, $url_self);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        if ($post) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        $result = curl_exec($ch);
        $error  = curl_error($ch);
        if ($error) {
            $result['curl_error'] = $error;

            return $result;
        }

        curl_close($ch);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    /**
     * @param $url
     * @param null $postData
     * @return mixed
     * @throws CurlException
     * @throws InvalidResponseException
     */
    private function request($url, $postData = null)
    {
        $response   = $this->cURL($url, $postData);
        $jsonResult = json_decode($response, true);

        if (null === $jsonResult || false === $jsonResult) {
            throw new InvalidResponseException('We got invalid response: ' . $response);
        } elseif (is_array($response) && in_array('curl_error', $response)) {
            throw new CurlException('We got curl error :' . $response);
        }

        return $jsonResult;
    }

    /**
     * @return string
     */
    private function getStreamUrl(): string
    {
        return getenv('API_STREAM_URL') . $this->channel;
    }

    /**
     * @return bool
     * @throws CurlException
     * @throws InvalidResponseException
     */
    private function isStreamOffline(): bool
    {
        $data = $this->request($this->getStreamUrl());
        if (null === $data['stream']) {
            return true;
        }

        return false;
    }
}