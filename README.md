# Test moneyfunny

Есть такой сервис потокового вещания - Twitch, у него есть API. 
Для тестового задания возьмите любой стрим в статусе online (желательно англоязычный). 
Задание состоит из трёх мини-заданий на проверку разных навыков.

## 1

К письму прилагается готовая функция для работы с API. 
1. Оберните её в класс, добавьте два своих метода. 
2. Первый должен обращаться к API и возвращать превью заданного стрима, т.е. картинку (в API увидите размер large). 
3. Второй должен возвращать HTML проигрывателя для заданного стрима, т.е. iframe (проигрыватели с Twitch встраиваются также, как любое видео с YouTube).

## 2 

1. Далее, выведите превью стрима на страницу. 
2. По центру картинки разместите кнопку проигрывания.
3. Использование CSS-фреймворков здесь нежелательно.

## 3 

1. Для этого пункта используйте jQuery. 
2. Нужно сделать, чтобы при нажатии на кнопку проигрывания, картинка "превращалась" в проигрыватель стрима. 
3. Эту задачу можно выполнить как минимум тремя способами. Реализация - полностью на ваше усмотрение.

## Функця для работы с API

```php
<?php
/**
 * Готовая функция для работы с Twitch v5 API
 * @param $url
 * @param $post
 * @return bool|mixed
 */
function cURL($url, $post) {
	$url_self = 'http';
	if ($_SERVER["HTTPS"] == "on") {
		$url_self .= "s";
	}
	$url_self .= "://".$_SERVER["SERVER_NAME"];
	if ($_SERVER["SERVER_PORT"] != "80") {
		$url_self .= ":".$_SERVER["SERVER_PORT"];
	}

	if (substr($url_self, -1) != '/') {
		$url_self = $url_self.'/';
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Client-ID: b3rudj39b54n6mdai2hndi84fets2tm'));
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	curl_setopt($ch, CURLOPT_REFERER, $url_self);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

	if ($post) {
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	}

	$result = curl_exec($ch);
	$error = curl_error($ch);
	if ($error) {
		$result['curl_error'] = $error;
		return $result;
	}

	curl_close($ch);
	if ($result) {
		return $result;
	} else {
		return false;
	}
}

?>
```