<?php

require_once 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$idList = explode('|', getenv('CHANNEL_ID_LIST'));

$twitch = new Main\TwitchStream($idList[0]);

foreach ($idList as $channelId) {
    try {
        $previewUrl = $twitch
            ->setChannel($channelId)
            ->getPreview()
        ;
    } catch (\Main\Exception\CurlException $e) {
        echo PHP_EOL . '<br/>' . ' got curl Exception: ' . $e->getMessage() . ' try to continue';
    }

    if (is_array($previewUrl) && in_array('offline', $previewUrl)) {
        echo $idList[0], ' ← stream is offline, getting next' . PHP_EOL;
        continue;
    }

    break;
}

include "index.tpl.php";
